package com.leisurepassgroup.product.controller;

import com.leisurepassgroup.product.model.Category;
import com.leisurepassgroup.product.model.Product;
import com.leisurepassgroup.product.repository.CategoryRepository;
import com.leisurepassgroup.product.repository.ProductRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DisplayName("Category Controller Integration Tests")
class CategoryControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private CategoryRepository categoryRepository;

    @Test
    public void should_return200withCategoryList_whenGetCategories() throws Exception {
        List<Category> mockedCategories = Collections.singletonList(
                Category.builder().name("test").id(1L).build()
        );
        doReturn(mockedCategories).when(categoryRepository)
                .findAll();
        mockMvc.perform(MockMvcRequestBuilders.get("/api/categories/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].name", is("test")))
                .andReturn();
    }


    @Test
    public void should_return200WithProduct_whenGetOldestLastPurchasedProduct() throws Exception {
        doReturn(Optional.of(Product.builder().id(1L).name("test").description("bar").build()))
                .when(productRepository)
                .findTopByCategoryIdAndLastPurchasedAtIsNotNullOrderByLastPurchasedAtAsc(eq(1L));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/categories/1/product/oldestLastPurchased")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("test")))
                .andExpect(jsonPath("$.description", is("bar")))
                .andReturn();
    }

}