package com.leisurepassgroup.product.controller;

import com.leisurepassgroup.product.dto.ProductDto;
import com.leisurepassgroup.product.model.Product;
import com.leisurepassgroup.product.repository.CategoryRepository;
import com.leisurepassgroup.product.repository.ProductRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@DisplayName("Product Controller Integration Tests")
class ProductControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private CategoryRepository categoryRepository;

    @Test
    public void should_return200withExistingProductId_whenGetProduct() throws Exception {
        doReturn(Optional.of(Product.builder().id(1L).name("test").description("bar").build()))
                .when(productRepository)
                .findById(eq(1L));
        testGetProduct(1, ProductDto.builder()
                .name("test")
                .description("bar")
                .build());
    }

    @Test
    public void should_return404withNonExistingProductId_whenGetProduct() throws Exception {
        doReturn(Optional.empty())
                .when(productRepository)
                .findById(eq(1L));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(404))
                .andReturn();
    }

    @Test
    public void should_return400withInvalidProductId_whenGetProduct() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/foo")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(400))
                .andReturn();
    }

    @Test
    public void should_return200withProductList_whenGetProducts() throws Exception {
        Page<Product> mockedPageProduct = new PageImpl<>(Collections.singletonList(
                Product.builder().name("test").id(1L).build()
        ));
        doReturn(mockedPageProduct).when(productRepository)
                .findByIsDeleted(eq(false), any());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(1)))
                .andReturn();
    }

    @Test
    public void should_return200withEmptyProductList_whenGetProducts() throws Exception {
        Page<Product> mockedPageProduct = Page.empty();
        doReturn(mockedPageProduct).when(productRepository)
                .findByIsDeleted(eq(false), any());
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", empty()))
                .andReturn();
    }

    @Test
    public void should_return200_whenDeleteProducts() throws Exception {
        doReturn(1).when(productRepository)
                .softDeleteProductById(eq(1L));
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void should_return200WithNoRowsWasDeleted_whenDeleteProducts() throws Exception {
        doReturn(0).when(productRepository)
                .softDeleteProductById(anyLong());
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/products/1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }

    private void testGetProduct(int id, ProductDto expectedProduct) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/products/" + id)
//                .with(user(TEST_USER_ID))
//                .with(csrf())
//                .content(birthday)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(id)))
                .andExpect(jsonPath("$.name", is(expectedProduct.getName())))
                .andExpect(jsonPath("$.description", is(expectedProduct.getDescription())))
                .andReturn();
    }

}