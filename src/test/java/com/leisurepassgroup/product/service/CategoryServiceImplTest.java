package com.leisurepassgroup.product.service;

import com.leisurepassgroup.product.dto.CategoryDto;
import com.leisurepassgroup.product.exception.ProductNotFoundException;
import com.leisurepassgroup.product.model.Category;
import com.leisurepassgroup.product.model.Product;
import com.leisurepassgroup.product.repository.CategoryRepository;
import com.leisurepassgroup.product.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

@SpringBootTest
class CategoryServiceImplTest {
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        modelMapper = new ModelMapper();
    }

    @Test
    void should_call_findAll_when_getAllCategories() {
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        doReturn(Arrays.asList(
                Category.builder().name("a").build(),
                Category.builder().name("b").build()
        )).when(categoryRepository).findAll();
        ProductRepository productRepository = mock(ProductRepository.class);
        CategoryService service = new CategoryServiceImpl(productRepository,
                categoryRepository, modelMapper);
        service.getAllCategories();
        verify(categoryRepository, times(1)).findAll();
    }

    @Test
    void should_call_findTopByCategoryIdOrderByLastPurchasedAtAsc_when_getOldestLastPurchasedAtProduct() {
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        ProductRepository productRepository = mock(ProductRepository.class);
        doReturn(Optional.of(Product.builder().name("test").lastPurchasedAt(Calendar.getInstance()).build())).when(productRepository).findTopByCategoryIdAndLastPurchasedAtIsNotNullOrderByLastPurchasedAtAsc(eq(1L));
        CategoryService service = new CategoryServiceImpl(productRepository,
                categoryRepository, modelMapper);
        service.getOldestLastPurchasedAtProduct(1L);
        verify(productRepository, times(1)).findTopByCategoryIdAndLastPurchasedAtIsNotNullOrderByLastPurchasedAtAsc(eq(1L));
    }

    @Test
    void should_throw_not_found_exception_when_getOldestLastPurchasedAtProduct() {
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        ProductRepository productRepository = mock(ProductRepository.class);
        CategoryService service = new CategoryServiceImpl(productRepository,
                categoryRepository, modelMapper);
        try {
            service.getOldestLastPurchasedAtProduct(1L);
            fail("should not reach here");
        } catch (Exception e) {
            assertThat(e, is(instanceOf(ProductNotFoundException.class)));
        }
        verify(productRepository, times(1))
                .findTopByCategoryIdAndLastPurchasedAtIsNotNullOrderByLastPurchasedAtAsc(eq(1L));
    }

    @Test
    void should_call_save_when_addCategory() {
        CategoryRepository categoryRepository = mock(CategoryRepository.class, RETURNS_MOCKS);
        ProductRepository productRepository = mock(ProductRepository.class);
        CategoryService service = new CategoryServiceImpl(productRepository,
                categoryRepository, modelMapper);
        service.addCategory(CategoryDto.builder().name("test a").build());
        verify(categoryRepository, times(1))
                .save(any(Category.class));
    }
}