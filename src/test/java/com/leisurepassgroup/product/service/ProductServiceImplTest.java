package com.leisurepassgroup.product.service;

import com.leisurepassgroup.product.dto.ProductDto;
import com.leisurepassgroup.product.exception.CategoryNotFoundException;
import com.leisurepassgroup.product.model.Category;
import com.leisurepassgroup.product.model.Product;
import com.leisurepassgroup.product.repository.CategoryRepository;
import com.leisurepassgroup.product.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProductServiceImplTest {
    ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        modelMapper = new ModelMapper();
    }

    @Test
    void should_call_findByIsDeleteWithFalse_when_getAllProducts() {
        ProductRepository productRepository = mock(ProductRepository.class, RETURNS_MOCKS);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        ProductService productService = new ProductServiceImpl(productRepository,
                categoryRepository,
                modelMapper);

        productService.getAllProducts(PageRequest.of(0, 5));
        // verify if the findByIsDeleted method is called when getAllProducts is called
        verify(productRepository, times(1))
                .findByIsDeleted(eq(false), any());
    }

    @Test
    void should_call_findById_when_getProduct() {
        ProductRepository productRepository = mock(ProductRepository.class, RETURNS_MOCKS);
        doReturn(Optional.of(Product.builder().name("test name").build())).when(productRepository).findById(any());
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        ProductService productService = new ProductServiceImpl(productRepository,
                categoryRepository,
                modelMapper);
        productService.getProduct(1L);
        verify(productRepository, times(1))
                .findById(eq(1L));
    }

    @Test
    void should_call_save_when_addProduct() {
        ProductRepository productRepository = mock(ProductRepository.class, RETURNS_MOCKS);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        doReturn(Optional.of(Category.builder().name("test category").id(1L).build())).when(categoryRepository).findById(eq(1L));
        ProductService productService = new ProductServiceImpl(productRepository,
                categoryRepository,
                modelMapper);
        productService.addProduct(
                ProductDto.builder().name("test")
                        .categoryId(1L)
                        .build());
        verify(productRepository, times(1)).save(any(Product.class));
    }

    @Test
    void should_raise_category_not_found_when_addProduct() {
        ProductRepository productRepository = mock(ProductRepository.class, RETURNS_MOCKS);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        ProductService productService = new ProductServiceImpl(productRepository,
                categoryRepository,
                modelMapper);
        try {
            productService.addProduct(
                    ProductDto.builder().name("test")
                            .categoryId(1L)
                            .build());
            fail("should not reach here");
        } catch(Exception e) {
            assertThat(e, is(instanceOf(CategoryNotFoundException.class)));
        }
        verify(productRepository, never()).save(any(Product.class));
    }



    @Test
    void should_call_softDeleteProductById_when_deleteProduct() {
        ProductRepository productRepository = mock(ProductRepository.class, RETURNS_MOCKS);
        CategoryRepository categoryRepository = mock(CategoryRepository.class);
        ProductService productService = new ProductServiceImpl(productRepository,
                categoryRepository,
                modelMapper);
        productService.deleteProduct(1L);
        verify(productRepository, times(1)).softDeleteProductById(eq(1L));
    }
}