package com.leisurepassgroup.product.util;

import com.leisurepassgroup.product.SharedMySQLContainer;
import com.leisurepassgroup.product.model.Category;
import com.leisurepassgroup.product.model.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
@Testcontainers
@ActiveProfiles("test")
class SchemaExporterIT {
    // will be shared between test methods
    @Container
    private static final SharedMySQLContainer container = SharedMySQLContainer.getInstance();

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    @Test
    public void test_exportToFile(@TempDir Path tempDir) throws IOException {
        Path file = tempDir.resolve("schema.sql");
        SchemaExporter exporter = SchemaExporter.builder()
                .jdbcUrl(container.getJdbcUrl())
                .username(container.getUsername())
                .password(container.getPassword())
                .entities(Arrays.asList(Product.class, Category.class))
                .build();
        exporter.exportToFile(file.toString());

        System.out.println(readFile(file.toString(), StandardCharsets.UTF_8));

        assertThat("File should exist", Files.exists(file), is(true));
    }
}