package com.leisurepassgroup.product.repository;

import com.leisurepassgroup.product.SharedMySQLContainer;
import com.leisurepassgroup.product.model.Category;
import com.leisurepassgroup.product.model.Product;
import com.mysql.cj.jdbc.MysqlDataSource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@Testcontainers
@ActiveProfiles("test")
class ProductRepositoryIT {
    // will be shared between test methods
    @Container
    private static final SharedMySQLContainer container = SharedMySQLContainer.getInstance();

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void testAddDeleteProduct() {
        Category c = new Category();
        c.setName("test category");
        categoryRepository.save(c);

        Product p = new Product();
        p.setName("Lpsum");
        p.setDescription("Lpsum");
        p.setCategory(c);
        Product saved = productRepository.save(p);

        assertThat(productRepository.findById(saved.getId()).orElseThrow(RuntimeException::new), allOf(
                hasProperty("id", is(notNullValue())),
                hasProperty("name", is("Lpsum")),
                hasProperty("description", is("Lpsum")),
                hasProperty("category", allOf(
                        hasProperty("id", is(notNullValue())),
                        hasProperty("name", is("test category"))
                )),
                hasProperty("createdAt", is(notNullValue())),
                hasProperty("updatedAt", is(notNullValue())),
                hasProperty("lastPurchasedAt", is(nullValue())),
                hasProperty("version", is(0))
        ));

        productRepository.deleteById(saved.getId());
        assertThat(productRepository.findById(saved.getId()).isPresent(), is(false));
    }

    @Test
    void testFindOldestLastPurchasedDateFromSingleCategory() {
        Optional<Product> product = productRepository.findTopByCategoryIdAndLastPurchasedAtIsNotNullOrderByLastPurchasedAtAsc(1);
        assertThat(product.isPresent(), is(true));
    }

    @Test
    void testSoftDeleteProduct() {
        assertThat(productRepository.findById(1L).orElseThrow(RuntimeException::new),
                allOf(hasProperty("deleted", is(false))));
        assertThat(productRepository.softDeleteProductById(1L), is(1));
        assertThat(productRepository.findById(1L).orElseThrow(RuntimeException::new),
                allOf(hasProperty("deleted", is(true))));
    }

    @NotNull
    private DataSource dataSource() {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUrl(container.getJdbcUrl());
        dataSource.setUser(container.getUsername());
        dataSource.setPassword(container.getPassword());
        return dataSource;
    }

}