package com.leisurepassgroup.product.repository;

import com.leisurepassgroup.product.SharedMySQLContainer;
import com.leisurepassgroup.product.model.Category;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@Testcontainers
@ActiveProfiles("test")
class CategoryRepositoryIT {
    // will be shared between test methods
    @Container
    private static final SharedMySQLContainer container = SharedMySQLContainer.getInstance();

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void testAddDeleteCategory() {
        Category c = new Category();
        c.setName("test category");
        Category saved = categoryRepository.save(c);
        assertThat(categoryRepository.findById(saved.getId()).orElseThrow(RuntimeException::new), allOf(
                hasProperty("id", is(notNullValue())),
                hasProperty("name", is("test category")),
                hasProperty("version", is(notNullValue()))
        ));
        categoryRepository.deleteById(saved.getId());
        assertThat(categoryRepository.findById(saved.getId()).isPresent(), is(false));
    }


}