package com.leisurepassgroup.product;

import org.testcontainers.containers.MySQLContainer;

public class SharedMySQLContainer extends MySQLContainer<SharedMySQLContainer> {
    private static final String IMAGE_VERSION = "mysql:5.7.22";

    static class LazyHolder {
        static SharedMySQLContainer INSTANCE = new SharedMySQLContainer();
    }

    private SharedMySQLContainer() {
        super(IMAGE_VERSION);
    }

    public static SharedMySQLContainer getInstance() {
        return LazyHolder.INSTANCE;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", LazyHolder.INSTANCE.getJdbcUrl());
        System.setProperty("DB_USERNAME", LazyHolder.INSTANCE.getUsername());
        System.setProperty("DB_PASSWORD", LazyHolder.INSTANCE.getPassword());
    }

    @Override
    public void stop() {
        super.stop();
    }
}
