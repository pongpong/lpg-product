create table categories (
   id bigint not null auto_increment,
    name varchar(15) not null,
    is_active BIT DEFAULT 1 not null,
    create_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP not null,
    update_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP not null,
    version bigint DEFAULT 0 not null,
    primary key (id)
) engine=InnoDB;

create table products (
    id bigint not null auto_increment,
    is_deleted BIT DEFAULT 0 not null,
    name varchar(30) not null,
    description varchar(100) not null,
    category_id bigint not null,
    last_purchased_at TIMESTAMP NULL DEFAULT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP not null,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP not null,
    version bigint DEFAULT 0 not null,
    primary key (id)
) engine=InnoDB;

alter table categories
   add constraint CATEGORIES_IDX1_NAME unique (name);
create index PRODUCTS_IDX2_IS_DELETED on products (is_deleted);
create index PRODUCTS_IDX3_CATEGORY_ID on products (category_id);

alter table products
   add constraint PRODUCTS_IDX1_NAME unique (name);

alter table products
add constraint FK_CATEGORY_ID
foreign key (category_id)
references categories (id);
