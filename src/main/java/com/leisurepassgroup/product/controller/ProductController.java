package com.leisurepassgroup.product.controller;

import com.leisurepassgroup.product.dto.ProductDto;
import com.leisurepassgroup.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/products")
@Validated
public class ProductController {
    private final ProductService productService;

    public ProductController(@Autowired ProductService productService) {
        this.productService = productService;
    }

    /**
     * Get a list of products
     *
     * @param pageable the pageable contains both page number and page size.
     * @return a list of products
     */
    @GetMapping(value = {"", "/"})
    public @NotNull Iterable<ProductDto> getProducts(
            @PageableDefault() Pageable pageable) {
        PageRequest pageReq = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(),
                pageable.getSort()
        );
        return productService.getAllProducts(pageReq);
    }

    /**
     * Get a single product by id
     *
     * @param id the product id
     * @return a {@link ProductDto} instance.
     */
    @GetMapping("/{id}")
    public ProductDto getProduct(@PathVariable Long id) {
        return productService.getProduct(id);
    }

    /**
     * Delete a single product by id
     *
     * @param id the product id
     */
    @DeleteMapping("/{id}")
    public int delete(@PathVariable String id) {
        return productService.deleteProduct(Long.parseLong(id));
    }
}
