package com.leisurepassgroup.product.controller;

import com.leisurepassgroup.product.dto.CategoryDto;
import com.leisurepassgroup.product.dto.ProductDto;
import com.leisurepassgroup.product.service.CategoryService;
import com.leisurepassgroup.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/categories")
@Validated
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductService productService;

    /**
     * Get a list of categories
     *
     * @return a list of categories
     */
    @GetMapping(value = {"", "/"})
    public @NotNull Iterable<CategoryDto> getCategories() {
        return categoryService.getAllCategories();
    }

    /**
     * Get the oldest last purchased product from a single category.
     *
     * @param id the category id
     * @return the oldest last purchased product.
     */
    @GetMapping(value = "/{id}/product/oldestLastPurchased")
    public @NotNull ProductDto getOldestLastPurchasedProduct(@PathVariable Long id) {
        return categoryService.getOldestLastPurchasedAtProduct(id);
    }
}

