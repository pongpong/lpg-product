package com.leisurepassgroup.product.repository;

import com.leisurepassgroup.product.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    /**
     * Find all products by deleted flag
     *
     * @param isDeleted products is deleted or not.
     * @param pageable  the pagination object.
     * @return a page of product
     */

    Page<Product> findByIsDeleted(boolean isDeleted, Pageable pageable);

    /**
     * Get the oldest last purchased date product from a single category.
     *
     * @param categoryId the given category id.
     * @return {@link Optional<Product>} a optional product
     */
    Optional<Product> findTopByCategoryIdAndLastPurchasedAtIsNotNullOrderByLastPurchasedAtAsc(long categoryId);

    /**
     * Soft delete the product by id
     *
     * @param productId the product id.
     */
    @Modifying
    @Query("update Product p set p.isDeleted = 1 where p.id = ?1")
    @Transactional
    int softDeleteProductById(long productId);
}
