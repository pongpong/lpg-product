package com.leisurepassgroup.product.repository;

import com.leisurepassgroup.product.model.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {
}
