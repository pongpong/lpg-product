package com.leisurepassgroup.product.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Calendar;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "products",
        indexes = {
                @Index(name = "PRODUCTS_IDX1_NAME", columnList = "name", unique = true),
                @Index(name = "PRODUCTS_IDX2_IS_DELETED", columnList = "isDeleted"),
                @Index(name = "PRODUCTS_IDX3_CATEGORY_ID", columnList = "category_id")
        }
)
@Data
public class Product {
    /**
     * Product id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, updatable = false, nullable = false)
    private Long id;

    /**
     * Name of the product
     */
    @NotBlank(message = "Product name is required")
    @Column(nullable = false, length = 30)
    @Basic(optional = false)
    @Size(min = 1, max = 30)
    private String name;

    /**
     * Category of the product
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    /**
     * Description of the product
     */
    @NotBlank(message = "Product description is required")
    @Column(nullable = false, length = 100)
    @Size(min = 1, max = 100)
    private String description;

    /**
     * Last purchased date, null if never purchased.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(columnDefinition = "TIMESTAMP NULL DEFAULT NULL")
    private Calendar lastPurchasedAt;

    /**
     * Is produce deleted or not , for soft deletion.
     */
    @NotNull
    @Column(nullable = false, columnDefinition = "BIT DEFAULT 0", length = 1)
    private boolean isDeleted = false;

    /**
     * Product creation date
     */
    @Column(nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createdAt;

    /**
     * Product last modification date.
     */
    @Column(nullable = false,
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar updatedAt;

    /**
     * Version field
     */
    @Version
    @Column(columnDefinition = "BIGINT DEFAULT 0", nullable = false)
    private int version;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category=" + category +
                ", description='" + description + '\'' +
                ", lastPurchasedAt=" + lastPurchasedAt +
                ", isDeleted=" + isDeleted +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", version=" + version +
                '}';
    }
}
