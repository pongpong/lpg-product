package com.leisurepassgroup.product.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Calendar;

@Entity
@Table(name = "categories",
        indexes = {
                @Index(name = "CATEGORIES_IDX1_NAME", columnList = "name", unique = true),
        }
)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(of = {"id"})
public class Category {
    /**
     * Category id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, updatable = false, nullable = false)
    private Long id;

    /**
     * Name of the category
     */
    @NotBlank(message = "Category name is required")
    @Column(nullable = false, length = 15)
    @Size(min = 1, max = 15)
    private String name;

    /**
     * The category is active or not
     */
    @NotNull
    @Column(nullable = false, columnDefinition = "BIT DEFAULT 1", length = 1)
    private boolean isActive = true;

    /**
     * The creation date of the category
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Calendar createAt;

    /**
     * The last modified date of the category
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false,
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Calendar updateAt;

    /**
     * The version field
     */
    @Version
    @Column(columnDefinition = "BIGINT DEFAULT 0", nullable = false)
    private long version = 0L;

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createAt=" + createAt +
                ", updateAt=" + updateAt +
                '}';
    }
}
