package com.leisurepassgroup.product.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.text.SimpleDateFormat;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ProductDto {
    private static final ThreadLocal<SimpleDateFormat> DATE_FORMAT = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm"));

    private Long id;

    @NotBlank(message = "Name is mandatory")
    @Pattern(regexp = "^[A-Za-z0-9][A-Za-z0-9-_ ]*$",
            message = "An alphanumeric product name is mandatory.")
    private String name;

    @NotBlank(message = "Description is mandatory")
    private String description;

    @NotBlank(message = "Category ID is mandatory")
    private Long categoryId;
}
