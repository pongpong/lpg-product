package com.leisurepassgroup.product.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CategoryDto {
    private Long id;
    @NotBlank(message = "Category name is mandatory")
    @Pattern(regexp = "^[A-Za-z0-9][A-Za-z0-9-_ ]*",
            message = "An alphanumeric category name is mandatory")
    private String name;
}
