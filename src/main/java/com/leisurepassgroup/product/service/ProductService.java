package com.leisurepassgroup.product.service;

import com.leisurepassgroup.product.dto.ProductDto;
import com.leisurepassgroup.product.exception.ProductNotFoundException;
import com.leisurepassgroup.product.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface ProductService {
    /**
     * Get all products.
     *
     * @param pageRequest the page request
     * @return a list of product
     */
    Page<ProductDto> getAllProducts(PageRequest pageRequest);

    /**
     * Get single product by id.
     * throw {@link ProductNotFoundException} if product is not found.
     *
     * @param id the product id
     * @return {@link Product} a product
     */
    ProductDto getProduct(long id);

    /**
     * Add a product
     *
     * @param product the product could like to be saved.
     * @return the saved product id
     */
    long addProduct(ProductDto product);

    /**
     * Delete a product by id.
     *
     * @param id the product id.
     * @return return 1 if product was deleted , otherwise 0
     */
    int deleteProduct(long id);
}
