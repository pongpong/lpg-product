package com.leisurepassgroup.product.service;

import com.leisurepassgroup.product.dto.CategoryDto;
import com.leisurepassgroup.product.dto.ProductDto;
import com.leisurepassgroup.product.exception.ProductNotFoundException;
import com.leisurepassgroup.product.model.Category;
import com.leisurepassgroup.product.model.Product;
import com.leisurepassgroup.product.repository.CategoryRepository;
import com.leisurepassgroup.product.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    private final ModelMapper modelMapper;

    public CategoryServiceImpl(
            @Autowired ProductRepository productRepository,
            @Autowired CategoryRepository categoryRepository,
            @Autowired ModelMapper modelMapper) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public Iterable<CategoryDto> getAllCategories() {
        return StreamSupport.stream(categoryRepository.findAll().spliterator(), false)
                .map(this::toCategoryDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto getOldestLastPurchasedAtProduct(long categoryId) {
        return productRepository.findTopByCategoryIdAndLastPurchasedAtIsNotNullOrderByLastPurchasedAtAsc(categoryId)
                .map(this::toProductDTO)
                .orElseThrow(ProductNotFoundException::new);
    }

    private ProductDto toProductDTO(Product product) {
        return modelMapper.map(product, ProductDto.class);
    }

    private CategoryDto toCategoryDTO(Category category) {
        return modelMapper.map(category, CategoryDto.class);
    }


    /**
     * Add a category
     *
     * @param dto the category to be saved
     * @return the id of a saved category
     */
    @Transactional
    @Override
    public long addCategory(CategoryDto dto) {
        Category category = new Category();
        category.setId(dto.getId());
        category.setName(dto.getName());
        Category saved = categoryRepository.save(category);
        return saved.getId();
    }

}
