package com.leisurepassgroup.product.service;

import com.leisurepassgroup.product.dto.ProductDto;
import com.leisurepassgroup.product.exception.CategoryNotFoundException;
import com.leisurepassgroup.product.exception.ProductNotFoundException;
import com.leisurepassgroup.product.model.Category;
import com.leisurepassgroup.product.model.Product;
import com.leisurepassgroup.product.repository.CategoryRepository;
import com.leisurepassgroup.product.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    private final CategoryRepository categoryRepository;

    private final ModelMapper modelMapper;

    public ProductServiceImpl(@Autowired ProductRepository productRepository,
                              @Autowired CategoryRepository categoryRepository,
                              @Autowired ModelMapper modelMapper) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.modelMapper = modelMapper;
    }

    @Transactional
    @Override
    public Page<ProductDto> getAllProducts(PageRequest pageRequest) {
        return productRepository.findByIsDeleted(false, pageRequest)
                .map(this::toProductDTO);
    }

    private ProductDto toProductDTO(Product product) {
        return modelMapper.map(product, ProductDto.class);
    }


    @Override
    public ProductDto getProduct(long id) {
        return productRepository.findById(id)
                .map(this::toProductDTO)
                .orElseThrow(ProductNotFoundException::new);
    }

    @Transactional
    @Override
    public long addProduct(ProductDto product) {
        Product p = new Product();
        p.setName(product.getName());
        p.setDescription(product.getDescription());
        Category category = categoryRepository.findById(product.getCategoryId())
                .orElseThrow(CategoryNotFoundException::new);
        p.setCategory(category);
        Product saved = productRepository.save(p);
        return saved.getId();
    }

    @Transactional
    @Override
    public int deleteProduct(long id) {
        return productRepository.softDeleteProductById(id);
    }

}
