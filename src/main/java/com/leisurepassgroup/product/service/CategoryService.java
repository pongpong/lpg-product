package com.leisurepassgroup.product.service;

import com.leisurepassgroup.product.dto.CategoryDto;
import com.leisurepassgroup.product.dto.ProductDto;

public interface CategoryService {
    /**
     * Add a category.
     *
     * @param category the category dto
     * @return the id of the added category
     */
    long addCategory(CategoryDto category);

    /**
     * Get a list of categories.
     *
     * @return a list of categories
     */
    Iterable<CategoryDto> getAllCategories();

    /**
     * Get the oldest last purchased date product from a single category.
     *
     * @return the {@link ProductDto} oldest last purchased date product.
     */
    ProductDto getOldestLastPurchasedAtProduct(long categoryId);
}
