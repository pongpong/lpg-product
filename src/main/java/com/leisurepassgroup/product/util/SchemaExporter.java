package com.leisurepassgroup.product.util;

import lombok.Builder;
import lombok.Singular;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.schema.TargetType;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

import java.util.EnumSet;
import java.util.Set;

@Builder
public class SchemaExporter {
    private static final String DEFAULT_DIALECT = "org.hibernate.dialect.MySQL57Dialect";
    private static final String DEFAULT_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String username;
    private final String password;
    private final String jdbcUrl;
    @Builder.Default
    private final String dialect = DEFAULT_DIALECT;
    @Builder.Default
    private final String driver = DEFAULT_DRIVER;
    @Singular
    private final Set<Class<?>> entities;

    private Metadata buildMetadata() {
        MetadataSources metadataSources = new MetadataSources(new StandardServiceRegistryBuilder()
                .applySetting(AvailableSettings.HBM2DDL_AUTO, "update")
                .applySetting(AvailableSettings.DIALECT, dialect)
                .applySetting(AvailableSettings.DRIVER, driver)
                .applySetting(AvailableSettings.JPA_JDBC_URL, jdbcUrl)
                .applySetting(AvailableSettings.USER, username)
                .applySetting(AvailableSettings.PASS, password)
                .applySetting(AvailableSettings.PHYSICAL_NAMING_STRATEGY,
                        SpringPhysicalNamingStrategy.class.getCanonicalName())
                .build());
        for (Class<?> entity : entities) {
            metadataSources.addAnnotatedClass(entity);
        }
        return metadataSources.buildMetadata();
    }

    public void exportToFile(String file) {
        SchemaExport schemaExport = new SchemaExport()
                .setDelimiter(";")
                .setFormat(true)
                .setOutputFile(file);
        schemaExport.createOnly(EnumSet.of(TargetType.SCRIPT),
                buildMetadata());
    }

    public void exportToStdOut() {
        SchemaExport schemaExport = new SchemaExport();
        schemaExport.setFormat(true);
        schemaExport.createOnly(EnumSet.of(TargetType.STDOUT),
                buildMetadata());
    }
}
