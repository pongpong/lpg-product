import csv
import datetime

with open("products.csv", "rb") as infile, open("products.sql", "wb") as outfile:
	reader = csv.reader(infile)
	next(reader, None)  # skip the headers
	outfile.write("INSERT INTO products(NAME, DESCRIPTION, CATEGORY_ID, CREATED_AT, UPDATED_AT, LAST_PURCHASED_AT) VALUES ")
	for i, row in enumerate(reader):
		row[4] = datetime.datetime.strptime(row[4], "%m/%d/%Y %H:%M").strftime("%Y-%m-%d %H:%M:%S")
		row[5] = datetime.datetime.strptime(row[5], "%m/%d/%Y %H:%M").strftime("%Y-%m-%d %H:%M:%S")
		row[6] = datetime.datetime.strptime(row[6], "%m/%d/%Y %H:%M").strftime("%Y-%m-%d %H:%M:%S")
		row = ["'%s'" % r.replace("'", "''") for r in row[1:]]
		outfile.write(",\n" if i else "\n")
		outfile.write("(")
		outfile.write(",".join(row))
		outfile.write(")")
	outfile.write(";\n")
